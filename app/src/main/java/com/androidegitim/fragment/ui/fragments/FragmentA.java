package com.androidegitim.fragment.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.fragment.R;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class FragmentA extends Fragment {


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        RDALogger.info("Fragment A - onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RDALogger.info("Fragment A - onCreate");
    }

    @Override
    public void onStart() {
        super.onStart();
        RDALogger.info("Fragment A - onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        RDALogger.info("Fragment A - onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        RDALogger.info("Fragment A - onPause");
    }

    @Override
    public void onStop() {
        super.onStop();

        RDALogger.info("Fragment A - onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        RDALogger.info("Fragment A - onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RDALogger.info("Fragment A - onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();

        RDALogger.info("Fragment A - onDetach");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RDALogger.info("Fragment A - onActivityCreated");

        Bundle bundle = getArguments();

        if (bundle != null && bundle.containsKey("test")) {

            RDALogger.info(getArguments().getString("test"));

        } else {

            RDALogger.info("veri gelmedi");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        RDALogger.info("Fragment A - onCreateView");

        return inflater.inflate(R.layout.fragment_a, container, false);
    }
}
