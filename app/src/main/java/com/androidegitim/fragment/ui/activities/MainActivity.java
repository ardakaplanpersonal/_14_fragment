package com.androidegitim.fragment.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.fragment.R;
import com.androidegitim.fragment.ui.fragments.FragmentA;
import com.androidegitim.fragment.ui.fragments.FragmentB;
import com.androidegitim.fragment.ui.fragments.FragmentC;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        RDALogger.start("UYGULAMA İSMİ").enableLogging(true);

        FragmentA fragmentA = new FragmentA();

        Bundle bundle = new Bundle();

        bundle.putString("test", "ben bir test cümlesiyim.");

        fragmentA.setArguments(bundle);

        openFragment(fragmentA);
    }


    @OnClick(R.id.main_button_fragment_a)
    public void openFragmentA() {

        openFragment(new FragmentA());
    }

    @OnClick(R.id.main_button_fragment_b)
    public void openFragmentB() {

        openFragment(new FragmentB());
    }

    @OnClick(R.id.main_button_fragment_c)
    public void openFragmentC() {

        openFragment(new FragmentC());
    }

    private void openFragment(Fragment openingFragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

        fragmentTransaction.replace(R.id.main_linearlayout_fragment_part, openingFragment);

        fragmentTransaction.addToBackStack(openingFragment.getClass().getName());

        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {

            finish();

        } else {

            super.onBackPressed();
        }
    }
}
